package business;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("/v1")
@Produces("application/json")
@Consumes("application/json")
public class BibleRestService {
	BibleService service = new BibleService();
	
	@GET
	@Path("/word-search/{word}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response wordSearch(@PathParam("word") String word) {
		String response = service.lookUpWord(word, false);
		if (response == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("{\"error\":\"No verses found for word " + word+ "\"}").build();
		}
		
		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}
	
	@GET
	@Path("/occurances/{word}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOccurances(@PathParam("word") String word) {
		String response = service.lookUpWord(word, true);
		if (response == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("{\"error\":\"No verses found for word " + word + "\"}").build();
		}
		
		return Response.ok("{\"count\":" + response + "}", MediaType.APPLICATION_JSON).build();
	}
	
	@GET
	@Path("/verse/{book}/{chapter}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVersesForBookAndChapter(@PathParam("book") String book, @PathParam("chapter") int chapter) {
		String response = service.lookupChapter(book, chapter);
		if (response == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("{\"error\":\"No book or verses found for book and chapter " + book + " " + chapter+ "\"}").build();
		}
		
		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}
	
	@GET
	@Path("/verse/{book}/{chapter}/{verse}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getVerseForBookAndChapter(@PathParam("book") String book, @PathParam("chapter") int chapter, @PathParam("verse") int verse) {
		String response = service.lookupVerse(book, chapter, verse);
		if (response == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("{\"error\":\"No book or verse found for book and chapter " + book + " " + chapter + ":" + verse+ "\"}").build();
		}
		
		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}
}
