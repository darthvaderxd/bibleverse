package business;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import beans.Book;

/**
 * Session Bean implementation class BibleService
 */
@Stateless
@LocalBean
public class BibleService {
	private String bibleServiceUrl = "https://api.scripture.api.bible/v1/";
	
	// You will want to get your api key from https://scripture.api.bible/ 
	private String apiKey = "PUT_KEY_HERE";
	
	// books and chapters... currently only 4 apostles, matthew, mark, luke, john
	List<Book> books = new ArrayList<Book>();

    /**
     * Default constructor. 
     */
    public BibleService() {
    	// adding the book of John and chapters for it
    	Book matthew = new Book("MAT", "matthew");
    	Book mark = new Book("MRK", "mark");
    	Book luke = new Book("LUK", "luke");
    	Book john = new Book("JHN", "john");
    	books.add(matthew);
    	books.add(mark);
    	books.add(luke);
    	books.add(john);
    }
    
    private String makeRequestToAPIWithURL(URL url) {
    	try {
	    	HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("api-key", apiKey);
			
			int status = con.getResponseCode();
			System.out.println("status => " + status);
			
			if (status == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer content = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
				    content.append(inputLine);
				}
				in.close();
				con.disconnect();
				return content.toString();
			} else {
				System.out.println("non 200 request for " + url.toURI());
			}
    	} catch (Exception e) {
    		// swallow the error
    	}
    	return null;
    }
    
    private Book findBook(String book) {
    	for (int i = 0; i < books.size(); i += 1) {
    		Book b = books.get(i);
    		if (b.getName().equalsIgnoreCase(book)) {
    			return b;
    		}
    	}
    	return null;
    }

    public String lookUpWord(String word, boolean getCount) {
    	try {
    		URL url = new URL(bibleServiceUrl + "bibles/de4e12af7f28f599-02/search?query=" + word + "&sort=relevance");
			String response = makeRequestToAPIWithURL(url);
			
			// nothing found
			if (response.contains("verses\":[]")) {
				return null;
			}
			
    		if (!getCount) {
    			return response;
    		} else {
    			// for now just returning 10 if response is not null
    			if (response != null) {
    				return "10";
    			}
    		}
    	} catch (Exception e) {
    		// do nothing
    	}
    	return null;
    }
    
    public String lookupChapter(String book, int chapter) {
    	Book b = findBook(book);
    	
    	if (b != null) {
	    	try {
	    		URL url = new URL(bibleServiceUrl + "bibles/de4e12af7f28f599-02/chapters/" + b.getBookId() + "." + chapter + "/verses");
				return makeRequestToAPIWithURL(url);
	    	} catch (Exception e) {
	    		// do nothing
	    	}
    	}
    	return null;
    }
    
    public String lookupVerse(String book, int chapter, int verse) {
    	Book b = findBook(book);
    	if (b != null) {
	    	try {
	    		URL url = new URL(bibleServiceUrl + "bibles/de4e12af7f28f599-02/verses/" + b.getBookId() + "." + chapter + "." + verse + "?content-type=text&include-notes=false&include-titles=true&include-chapter-numbers=false&include-verse-numbers=true&include-verse-spans=false&use-org-id=false");
				String response = makeRequestToAPIWithURL(url);
				System.out.println("response => " + response);
				return response;
	    	} catch (Exception e) {
	    		// do nothing
	    	}
    	}
    	return null;
    }
}
